@extends('backend.layouts.app_layout')

@section('section')

<div class="page-content">

    @include('backend.includes.breadcrumb')


    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
<div class="card">
  <div class="card-body">
    <h6 class="card-title">Product Table</h6>
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{$message}}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="btn-close"></button>
        </div>
    @endif
    <div>
        <a href="{{url('add-product')}}"><button type="button" class="btn btn-primary float-end">ADD PRODUCT</button></a>
    </div>
    <div class="table-responsive">
      <table id="dataTableExample" class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Status</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
            <?php $i=1 ?>
          @foreach ($data as $item)
        <tr>
            <td>{{$i}}</td>
            <td>{{$item->name}}</td>
            <td>{{$item->status}}</td>
            <td>{{$item->price}}</td>
            <td>{{$item->quantity}}</td>
            <td>{{$item->image}}</td>
            <?php $i++ ?>
            <td>
                <a href="{{url('view-product')}}"><span class="m-1 text-primary"> <i data-feather="eye"></i></span></a>
                <a href="{{url('edit-product')}}"><span class="m-1 text-warning"><i data-feather="edit"></i></span></a>
                <a href="{{url('delete-product')}}"><span class="m-1 text-danger"> <i data-feather="trash-2"></i></span></a>
            </td>
        </tr>
        @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
        </div>
    </div>
</div>
@endsection
