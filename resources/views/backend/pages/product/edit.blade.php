@extends('backend.layouts.app_layout')

@section('section')

<div class="page-content">

    @include('backend.includes.breadcrumb')

    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Edit Product</h4>
                    <form id="signupForm">
                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input id="name" class="form-control" name="name" type="text">
                        </div>

                        <div class="mb-3">
                            <label for="category" class="form-label">Category</label>
                            <select class="form-select" name="category" id="category">
                                <option selected disabled>Select your category</option>
                                <option>Ethnic</option>
                                <option>Western</option>
                                <option>Classic</option>
                                <option>Traditional</option>
                                <option>Formal</option>
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="quantity" class="form-label">Price</label>
                            <input id="quantity" class="form-control" name="quantity" type="text">
                        </div>

                        <div class="mb-3">
                            <label for="quantity" class="form-label">Quantity</label>
                            <input id="quantity" class="form-control" name="quantity" type="text">
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Status</label>
                            <div>
                                <div class="form-check form-check-inline">
                                    <input type="radio" class="form-check-input" value="A" name="status" id="active" checked>
                                    <label class="form-check-label" for="active">
                                    Active
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input type="radio" class="form-check-input form-check-input-danger" value="I" name="status" id="inactive">
                                    <label class="form-check-label" for="inactive">
                                    Inactive
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label class="form-label" for="formFile">Product Image</label>
                            <input class="form-control" type="file" id="formFile">
                        </div>

                        <input class="btn btn-primary" type="submit" value="Submit">
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>


@endsection
