@extends('backend.layouts.app_layout')


@section('section')
    <div class="page-content">
        @if (session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}

            </div>
        @endif

        @include('backend.includes.breadcrumb')
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add Category</h4>

                        <form id="signupForm" action="{{ url('save_category') }}" method="POST">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label">Name</label>
                                <input id="name" class="form-control" name="name" type="text">
                            </div>


                            <div class="mb-3">
                                <label class="form-label" name="status">Status</label>
                                <div>
                                    <div class="form-check form-check-inline">
                                        <input type="radio" class="form-check-input" name="status" value="A"
                                            id="active" checked>
                                        <label class="form-check-label" for="active">
                                            Active
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input type="radio" class="form-check-input" name="status" value="I"
                                            id="inactive">
                                        <label class="form-check-label" for="inactive">
                                            Inactive
                                        </label>
                                    </div>

                                </div>
                            </div>


                            <input class="btn btn-primary" type="submit" value="Submit">
                        </form>
                    </div>
                </div>
            </div>
        @endsection
