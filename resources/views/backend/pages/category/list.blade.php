@extends('backend.layouts.app_layout')

@section('section')
    <div class="page-content">

        @include('backend.includes.breadcrumb')
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">Category Table</h6>

                        <a href="{{ url('add') }}"><button type="button" class="btn btn-primary mb-1 mb-md-0 float-end">Add
                                Category</button></a>
                        <div class="table-responsive">

                            <table id="dataTableExample" class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Tiger Nixon</td>

                                        <td>pending</td>
                                        <td>
                                            <a href="{{ url('add') }}"><span class="m-1 text-primary"> <i
                                                        data-feather="eye"></i></span></a>
                                            <a href="{{ url('edit') }}"><span class="m-1 text-warning"> <i
                                                        data-feather="edit"></i></span></a>
                                            <a href="{{ url('delete') }}"><span class="m-1 text-danger"> <i
                                                        data-feather="trash-2"></i></span></a>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>2</td>
                                        <td>Garrett Winters</td>

                                        <td>deliver</td>
                                        <td>
                                            <span class="m-1 text-primary"> <i data-feather="eye"></i></span>
                                            <span class="m-1 text-warning"> <i data-feather="edit"></i></span>
                                            <span class="m-1 text-danger"> <i data-feather="trash-2"></i></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Ashton Cox</td>

                                        <td>shipping</td>
                                        <td>
                                            <span class="m-1 text-primary"> <i data-feather="eye"></i></span>
                                            <span class="m-1 text-warning"> <i data-feather="edit"></i></span>
                                            <span class="m-1 text-danger"> <i data-feather="trash-2"></i></span>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
