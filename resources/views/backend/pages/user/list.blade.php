@extends('backend.layouts.app_layout')

@section('section')

<div class="page-content">

    @include('backend.includes.breadcrumb')





    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h6 class="card-title">Customer Table</h6>
                    <!-- <p class="text-muted mb-3">Read the <a href="https://datatables.net/" target="_blank"> Official DataTables Documentation </a>for a full list of instructions and other options.</p> -->
                    <div class="table-responsive">
                        <table id="dataTableExample" class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Age</th>
                                    <th>Category</th>
                                    <th>Cart Value</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Deepak</td>
                                    <td>21</td>
                                    <td>Male</td>
                                    <td>₹ 6100</td>
                                    <td>
                                        <a href="{{ url('view') }}"><span class="m-1 text-primary"> <i data-feather="eye"></i></span></a>
                                        <a href="{{ url('update') }}"><span class="m-1 text-warning"> <i data-feather="edit"></i></span></a>
                                        <a href="{{ url('delete') }}"><span class="m-1 text-danger"> <i data-feather="trash-2"></i></span></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Deepak</td>
                                    <td>21</td>
                                    <td>Male</td>
                                    <td>₹ 6100</td>
                                    <td>
                                        <a href="{{ url('view') }}"><span class="m-1 text-primary"> <i data-feather="eye"></i></span></a>
                                        <a href="{{ url('update') }}"><span class="m-1 text-warning"> <i data-feather="edit"></i></span></a>
                                        <a href="{{ url('delete') }}"><span class="m-1 text-danger"> <i data-feather="trash-2"></i></span></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Deepak</td>
                                    <td>21</td>
                                    <td>Male</td>
                                    <td>₹ 6100</td>
                                    <td>
                                        <a href="{{ url('view') }}"><span class="m-1 text-primary"> <i data-feather="eye"></i></span></a>
                                        <a href="{{ url('update') }}"><span class="m-1 text-warning"> <i data-feather="edit"></i></span></a>
                                        <a href="{{ url('delete') }}"><span class="m-1 text-danger"> <i data-feather="trash-2"></i></span></a>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection