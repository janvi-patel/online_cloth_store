<nav class="sidebar">
    <div class="sidebar-header">
      <a href="#" class="sidebar-brand">
        Tridhya<span>Tech</span>
      </a>
      <div class="sidebar-toggler not-active">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
    <div class="sidebar-body">
      <ul class="nav">
        <li class="nav-item nav-category">Main</li>
        <li class="nav-item">
          <a href="{{url('dashboard')}}" class="nav-link">
            <i class="link-icon" data-feather="box"></i>
            <span class="link-title">Dashboard</span>
          </a>
        </li>
        <li class="nav-item">
            <a href="{{url('user')}}" class="nav-link">
              <i class="link-icon" data-feather="user"></i>
              <span class="link-title">Users</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('product')}}" class="nav-link">
              <i class="link-icon" data-feather="aperture"></i>
              <span class="link-title">Product</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('category')}}" class="nav-link">
              <i class="link-icon" data-feather="grid"></i>
              <span class="link-title">Category</span>
            </a>
          </li>


      </ul>
    </div>
  </nav>

