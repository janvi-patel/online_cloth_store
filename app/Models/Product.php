<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    public function deleteProduct(){

    }

    public function getDetails(){
        return Product::select()
                ->where('is_deleted','N')
                ->get();
    }


    public function saveProduct($req){

        $img = $req->file('image')->getClientOriginalName();

        $product = new Product;
        $product->name = $req->name;
        $product->category_id = $req->category_id;
        $product->price = $req->price;
        $product->quantity = $req->quantity;
        $product->status = $req->status;
        $product->image = $img;
        $product->is_deleted = 'N';

        $product->save();

        return "success";

        // print_r($product);
        // die();



    }
}
