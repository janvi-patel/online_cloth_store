<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Foundation\Auth\Admin as Authenticable;

class Admin extends Model
{
    use HasFactory , HasFactory , Notifiable;

    protected $guard = 'admin';
   
    public $table = 'admin';
    
     protected $fillable = [
        'name',
        'email',
        'password',
     ];

     //attribute that should hidden for seialization

     protected $hidden = [
        'password',
        'remember_token',
     ];

     protected $casts = [
        'email_varified_at' => 'datetime',
     ];

}
