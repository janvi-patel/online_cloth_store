<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class categoryController extends Controller
{
    public function add_category(Request $request)
    {
        return view('backend.pages.category.add');        //return view('backend.pages.category.add');
    }
    public function save_category(Request $request)
    {
        $data = new Category;
        $data->name = $request->name;
        $data->status = $request->status;
        $data->save();
        return redirect()->back()->with('success', 'Successfuly Added');        //return view('backend.pages.category.add');
     

    }
    public function delete_category()
    {
        return view('backend.pages.category.delete');
    }
    public function edit_category()
    {
        return view('backend.pages.category.edit');
    }
}
