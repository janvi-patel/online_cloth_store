<?php

namespace App\Http\Controllers\backend;

use App\Models\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class productController extends Controller
{
    public function add(){
        return view('backend.pages.product.add');
    }

    public function edit(){
        return view('backend.pages.product.edit');
    }

    public function index(){

        $obj = new Product;
        $data = $obj->getDetails();
        // print_r($data);
        // die();

        return view('backend.pages.product.list',['data'=> $data] );
    }

    public function view(){
        return view('backend.pages.product.view');
    }

    public function delete(){
        $obj = new Product;
        $res = $obj->deleteProduct();
        return redirect('product');
    }

    public function save_product(Request $req){
        $obj = new Product;

        $res = $obj->saveProduct($req);

        if($res=="success"){
            return redirect('product')->with('success','Product added successfully!');
        }
    }

}
