<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('category_id');
            $table->bigInteger('price');
            $table->bigInteger('quantity');
            $table->enum('status',['A','I'])->comment('A for Active & I for Inactive');
            $table->string('image')->nullable();
            $table->enum('is_deleted',['Y','N'])->comment('Y for Yes &  N for No');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
