<?php
 
namespace Database\Seeders;
 
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Admin;



 
class Adminseeder extends Seeder
{
    /**
     * Run the database seeders.
     */
    public function run()
    {
        // DB::table('admin')->insert([
        //     'name' =>'Sejal Sanodiya',
        //     'email' => 'sanodiyasejal2002@gmail.com',
        //     'password' => ('Sejal@123'),
        // ]);

        $admin = [
            'name' => 'Admin',
            'email'=> 'admin@gmail.com',
            'password'=> bcrypt('password')
        ];
        Admin::create($admin);
    }

    /**
     */
    public function __construct() {
    }
}