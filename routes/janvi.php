<?php

use App\Http\Controllers\backend\productController;
use Illuminate\Support\Facades\Route;


Route::get('product',[productController::class, 'index'])->name('product');
Route::get('add-product',[productController::class, 'add'])->name('add-product');
Route::get('edit-product',[productController::class, 'edit'])->name('edit-product');
Route::get('view-product',[productController::class, 'view'])->name('view-product');
Route::get('delete-product',[productController::class, 'delete'])->name('delete-product');

Route::post('save-product',[productController::class, 'save_product'])->name('save-product');

