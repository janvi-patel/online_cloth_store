<?php


use App\Http\Controllers\Admin\AdminAuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;

Route::get('/', function () {
    return view('welcome');
});




Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__.'/auth.php';
// Route::view('dashboard', 'backend.pages.dashboard');

Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function(){
 Route::namespace('Auth')->middleware('guest::admin')->group(function(){
    //login route

    Route::get('login','AuthenticatedSessionController@create')->name('login');
    Route::post('login','AuthenticatedSessionController@store')->name('adminlogin');
 });

 Route::middleware('admin')->group(function () {
    Route::get('dashboard','HomeController@index')->name('dashboard');
 });

 Route::post('logout','Auth\AuthenticatedSessionController@destroy')->name('logout');
});







