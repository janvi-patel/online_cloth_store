<?php

use Illuminate\Support\Facades\Route;



Route::get('/user', function () {
    return view('backend.pages.user.list');
});

Route::get('/view', function () {
    return view('backend.pages.user.view');
});

Route::get('/update', function () {
    return view('backend.pages.user.update');
});

Route::get('/delete', function () {
    return view('backend.pages.user.delete');
});
Route::get('/dashboard', function () {
    return view('backend.pages.dashboard');

});



